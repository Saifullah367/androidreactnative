import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
const Component2Screen =() => {
  return (
    <>
      
        <View  style={styles.center}>
          <Text>React Native 2nd Component</Text>
        </View>
    </>
  );
};


const styles = StyleSheet.create({
  center:{
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    marginTop : 100
    

  },
});

export default Component2Screen;

