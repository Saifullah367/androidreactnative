import { AppRegistry } from 'react-native';
import Component1Screen from './Component1Screen';
import Component2Screen from './Component2Screen';

AppRegistry.registerComponent('Component1Screen', () => Component1Screen)
AppRegistry.registerComponent('Component2Screen', () => Component2Screen)